import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { InicioComponent } from './pages/inicio/inicio.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { HomeComponent } from './pages/home/home.component';
import { SolicitudesComponent } from './pages/solicitudes/solicitudes.component';
import { PublicacionComponent } from './pages/publicacion/publicacion.component';


const routes: Routes = [
  {path: '', component: LoginComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'inicio', component: InicioComponent},
  {path: 'home', component: HomeComponent},
  {path: 'solicitud', component: SolicitudesComponent},
  {path: 'publicacion', component: PublicacionComponent},



];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

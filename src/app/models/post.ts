export interface PostInterface {
  titulo ?: string;
  descripcion ?: string;
  foto ?: string;
  id ?: string;
  userUid ?: string;
}

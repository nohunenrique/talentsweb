export interface Roles{
  normal ?: Boolean;
  editor ?: Boolean;
  admin ?: Boolean;
}

export interface UserInterface {
  id ?: string;
   name ?: string;
   ciudad ?: string;
  // telefono ?: string;
  email ?: string;
  password ?: string;
  roles ?: Roles;
  solicitud ?: string;
}

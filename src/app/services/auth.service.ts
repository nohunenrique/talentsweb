import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { map } from 'rxjs/operators';
import {AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { UserInterface } from '../models/user';
import { Observable } from 'rxjs/internal/Observable';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user$: Observable<UserInterface>;
  constructor(private afsAuth: AngularFireAuth, private afs: AngularFirestore) { }
  registerUser(name: string,ciudad: string ,email: string, pass: string, edit:boolean) {
    return new Promise((resolve, reject) => {
      this.afsAuth.createUserWithEmailAndPassword(email, pass)
      .then(userData => {resolve(userData),

        this.updateUserData(userData.user, name, ciudad, edit);
        console.log(name);
        console.log(ciudad);


      }).catch(err => console.log(reject(err)));

    });
  }
  loginEmailUser(email: string, pass: string) {
    return new Promise((resolve, reject) => {
      this.afsAuth.signInWithEmailAndPassword(email, pass)
      .then(userData => resolve(userData),
      err => reject(err));
    });
  }
  logoutUser() {
    return this.afsAuth.signOut();
  }
  isAuth() {
    return this.afsAuth.authState.pipe(map(auth => auth));
  }
   private updateUserData(user , name: string , ciudad:string, edit: boolean) {
     const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
     const data: UserInterface = {
       id: user.uid,
       name: name,
       ciudad: ciudad,
       // ciudad: user.ciudad,
     // telefono: user.telefono,
       email: user.email,
       roles: {
        normal:edit
         // normal: role

       },
       solicitud: ''

    }

     return userRef.set(data, { merge: true} );
   }



  isUserEditor(userUid){
    return this.afs.doc<UserInterface>(`users/${userUid}`).valueChanges();
  }






  canRead(user: UserInterface): boolean {
    const allowed = ['admin', 'editor', 'normal']
    return this.checkAuthorization(user, allowed)
  }

  canEdit(user: UserInterface): boolean {
    const allowed = ['admin', 'editor']
    return this.checkAuthorization(user, allowed)
  }

  canDelete(user: UserInterface): boolean {
    const allowed = ['admin']
    return this.checkAuthorization(user, allowed)
  }



  // determines if user has matching role
  private checkAuthorization(user: UserInterface, allowedRoles: string[]): boolean {
    if (!user) return false
    for (const role of allowedRoles) {
      if ( user.roles[role] ) {
        return true
      }
    }
    return false
  }

}

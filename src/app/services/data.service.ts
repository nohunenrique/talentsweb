import { Injectable } from '@angular/core';
import { AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument } from '@angular/fire/firestore';
import { UserInterface } from '../models/user';
import { Observable } from 'rxjs/internal/Observable';
import { map } from 'rxjs/operators';
import { PostInterface } from '../models/post';
@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private afs: AngularFirestore) { }
  private usersCollection: AngularFirestoreCollection<UserInterface>;
  private postCollection: AngularFirestoreCollection<PostInterface>;

  private books: Observable<UserInterface[]>;
  private userDoc: AngularFirestoreDocument<UserInterface>;
  private postDoc: AngularFirestoreDocument<PostInterface>;

  private user: Observable<UserInterface>;
  public selectedUser: UserInterface = {
    id: null
  };
  public selectedPost: PostInterface = {
    id: null
  };
  public users:UserInterface;

  getAllBooks() {
    this.usersCollection = this.afs.collection<UserInterface>('users');
    return this.books = this.usersCollection.snapshotChanges()
      .pipe(map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as UserInterface;
          data.id = action.payload.doc.id;
          return data;
        });
      }));
  }
  getAllPosts() {
    this.usersCollection = this.afs.collection<PostInterface>('Post');
    return this.books = this.usersCollection.snapshotChanges()
      .pipe(map(changes => {
        return changes.map(action => {
          const data = action.payload.doc.data() as PostInterface;
          data.id = action.payload.doc.id;
          return data;
        });
      }));
  }
  updateBook(user: UserInterface): void {
    let idUser = user.id;
    this.userDoc = this.afs.doc<UserInterface>(`users/${idUser}`);
    this.userDoc.update(user);
  }
  addBook(book: UserInterface): void {
    this.usersCollection.add(book);
  }
  getOneBook(idUser: string) {
    this.userDoc = this.afs.doc<UserInterface>(`users/${idUser}`);
    return this.user = this.userDoc.snapshotChanges().pipe(map(action => {
      if (action.payload.exists === false) {
        return null;
      } else {
        const data = action.payload.data() as UserInterface;
        data.id = action.payload.id;

        return data;
      }
    }));
  }
  addPost(post: PostInterface): void {
    this.postCollection.add(post);
  }
  newPost(userUid, displayname, description, foto) {
    return this.afs.collection('Post').add({
      usuario: userUid,
      name: displayname,
      descripcion: description,
      imagen: foto
    });
  }


}

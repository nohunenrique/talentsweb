import { Component, OnInit, ViewChild, ElementRef, Input  } from '@angular/core';
import { DataService } from '../../services/data.service';
import { UserInterface } from '../../models/user';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { AuthService } from '../../services/auth.service';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/Operators';
import { Observable } from 'rxjs/internal/Observable';
@Component({
  selector: 'app-solicitudes',
  templateUrl: './solicitudes.component.html',
  styleUrls: ['./solicitudes.component.css']
})
export class SolicitudesComponent implements OnInit {
@ViewChild('videoUser') inputVideoUser: ElementRef;
public url = '';
public userUid: string = null;
uploadPercent: Observable<number>;
urlVideo: Observable<string>;

  constructor(private route: Router, public authService: AuthService, private afs: AngularFirestore, private storage: AngularFireStorage) { }


  ngOnInit(): void {

  }
  onUpload(e) {
    // console.log('subir', e);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `videos/video_${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe( finalize(() => this.urlVideo = ref.getDownloadURL())).subscribe();
  }

  updateSoli() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
          this.userUid = auth.uid;
          const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${this.userUid}`);
          const data: UserInterface = {
            solicitud: this.inputVideoUser.nativeElement.value
        };
          return userRef.update(data);

      }
    });
    alert('La solicitud ya fue enviada');
    this.route.navigateByUrl('inicio');

  }


}

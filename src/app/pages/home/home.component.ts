import { Component, OnInit } from '@angular/core';
import { UserInterface } from '../../models/user';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  public Users: UserInterface[];

  constructor() { }

  ngOnInit(): void {
  }

}

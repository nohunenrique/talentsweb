import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  public app_name: string = 'Talento en America';
  public isLogged: boolean = false;

  constructor(private authService: AuthService) { }

  ngOnInit(): void {
    this.getCurrentUser();
  }
  getCurrentUser() {
    this.authService.isAuth().subscribe(auth=>{
      if (auth) {
        console.log('usuario logueado');
        this.isLogged = true;
      } else {
        console.log('Usuario no logueado');
        this.isLogged = false;
      }
    });
  }
  onLogout() {
    this.authService.logoutUser();
  }
}

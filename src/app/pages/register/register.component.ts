import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { AngularFireDatabase, AngularFireObject } from '@angular/fire/database';
import { identifierModuleUrl } from '@angular/compiler';
import { UserInterface } from '../../models/user';
import { DataService } from '../../services/data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  itemRef: AngularFireObject<any>;
  ref;
  constructor(private authService: AuthService, private route: Router, private data: AngularFireDatabase, public dataS: DataService) {
    this.itemRef = data.object('User');
    this.ref = this.data.database.ref('/user');
   }
  public nombre: string;
public user: UserInterface;
public email: string;
public password: string;
public confirmpassword: string;
// public ciudad: string = '';
public telefono: string;
public tipoUsuario: string;
public edit;
public admin;
public normal;

ciudad;
public isError = false;
  public msgError = '';
  ngOnInit(): void {
  }
  onAddUser() {
    console.log(this.ciudad);
    if (!this.email || !this.password || !this.confirmpassword) {
      alert('Necesita llenar todos los campos');

      return;
    } else if (this.password != this.confirmpassword) {

      alert('Las contraseñas no coinciden');
      return;

    }
    if (this.tipoUsuario === 'Talento') {
      this.edit = false;
    } else {
      this.edit = true;
    }
    this.authService.registerUser(this.nombre, this.ciudad, this.email, this.password, this.edit)
    .then((res) => {

       if (this.tipoUsuario === 'Talento') {
        this.route.navigateByUrl(`solicitud`);
      } else if (this.tipoUsuario === 'Normal') {
     this.route.navigateByUrl('inicio');
      }

    })
    .catch(err => console.log('err', err.message));

  }

}






import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(public afAuth: AngularFireAuth, private router: Router, private authSevice: AuthService) { }
  public email: string = '';
  public password: string = '';
  public isError = false;
  public msgError = '';
  ngOnInit(): void {
  }
  onLogin(): void {
    console.log('usuario ', this.email);
    console.log('password', this.password);
    this.authSevice.loginEmailUser(this.email, this.password)
    .then((res) => {
      this.router.navigate(['inicio']);
    }).catch(err => console.log('error', err.message));
  }

}

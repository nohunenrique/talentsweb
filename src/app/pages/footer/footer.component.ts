import { Component, OnInit } from '@angular/core';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';

import { AuthService } from '../../services/auth.service';
import { Observable } from 'rxjs/internal/Observable';
import { UserInterface } from '../../models/user';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  postRef: AngularFirestoreDocument<any>
  post$: Observable<any>;
  user: UserInterface;

  constructor(private afs: AngularFirestore, public auth: AuthService) { }

  ngOnInit(): void {
    this.postRef = this.afs.doc('posts/post');
    this.post$ = this.postRef.valueChanges();

    this.auth.user$
  }
  editPost() {
    this.postRef.update({ title: 'Edited Title!'});
  }

  deletePost() {
    this.postRef.delete();
  }

}

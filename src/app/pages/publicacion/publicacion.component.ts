import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { DataService } from '../../services/data.service';
import { PostInterface } from '../../models/post';
import { NgForm } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { AngularFirestoreDocument, AngularFirestore } from '@angular/fire/firestore';
import { UserInterface } from '../../models/user';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs/internal/Observable';
import { finalize } from 'rxjs/Operators';
@Component({
  selector: 'app-publicacion',
  templateUrl: './publicacion.component.html',
  styleUrls: ['./publicacion.component.css']
})
export class PublicacionComponent implements OnInit {
@ViewChild('videoUser') inputVideoUser: ElementRef;

public descripcion: string;
public userUid: string = null;
public userI: UserInterface = {};
public datos;
uploadPercent: Observable<number>;
urlVideo: Observable<string>;
  constructor(private data: DataService, public authService: AuthService, private afs: AngularFirestore, private storage: AngularFireStorage) { }
private post: PostInterface = {};
  ngOnInit(): void {
  }

  onUpload(e){
    console.log(e);
    const id = Math.random().toString(36).substring(2);
    const file = e.target.files[0];
    const filePath = `publicacion/publicacion_${id}`;
    const ref = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);
    this.uploadPercent = task.percentageChanges();
    task.snapshotChanges().pipe( finalize(() => this.urlVideo = ref.getDownloadURL())).subscribe();

  }
  publicar(): void {
    // Guardar publicación
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
          this.userUid = auth.email;
          this.userI.name;
          // this.datos=this.afs.doc(`users/${this.userUid}`);


          this.data.newPost( this.userUid, this.userI , this.descripcion, this.inputVideoUser.nativeElement.value);

      }
    });
    alert('La solicitud ya fue enviada');



  }
}

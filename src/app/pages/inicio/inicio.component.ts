import { Component, OnInit, Pipe, PipeTransform, Input } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { UserInterface } from '../../models/user';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { DataService } from '../../services/data.service';
import { Observable } from 'rxjs/internal/Observable';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import * as admin from 'firebase';
import { PostInterface } from '../../models/post';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit{
  constructor(private authService: AuthService, private afsAuth: AngularFireAuth, private afs: AngularFirestore, private dataApi: DataService, public sanitizer: DomSanitizer) {
  }
  public Users: UserInterface[];
  public post: PostInterface[];
  public isAdmin: any = null;
  public isEditor: boolean;
  public isNormal :boolean;
  public userUid: string = null;
ngOnInit(): void {

  this.getCurrentUser();
  this.getListBooks();
  this.getListPost();
  }

  onLogout() {
    this.afsAuth.signOut();
  }

  getCurrentUser() {
    this.authService.isAuth().subscribe(auth => {
      if (auth) {
        this.userUid = auth.uid;
        this.authService.isUserEditor(this.userUid).subscribe(userRole => {
          this.isAdmin = Object.assign({}, userRole.roles).hasOwnProperty('normal');

          // this.isAdmin = false;


          // this.isAdmin=true;
          // this.isAdmin = this.isAdmin.hasOwnProperty('admin');
        });
      }
      if(auth){
        this.userUid = auth.uid;
        this.authService.isUserEditor(this.userUid).subscribe(userRole => {
          this.isEditor = Object.assign({}, userRole.roles).hasOwnProperty('admin');
        });
      }
    });
  }
  getListPost() {
    this.dataApi.getAllPosts()
      .subscribe(posts => {
        this.post = posts;
      });
  }
  getListBooks() {
    this.dataApi.getAllBooks()
      .subscribe(user => {
        this.Users = user;
      });
  }
  solicitud(user : UserInterface){
    console.log('Usuario', user);
    this.dataApi.selectedUser = Object.assign({}, user);
  }
  updateSoli(user: UserInterface) {
    let FieldValue = admin.firestore.FieldValue;
    let fieldRef:  AngularFirestoreDocument<any> = this.afs.doc(`users/${user.id}`);
    let removeField = fieldRef.set({'roles':
    {['normal'] : FieldValue.delete(), ['editor']: true}}, {merge: true});
  }


}

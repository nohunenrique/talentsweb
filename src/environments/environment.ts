// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCro0p2cXh8qgTsFd3rrxWgXCPl_zN10bA",
    authDomain: "apptalents-bfdcf.firebaseapp.com",
    databaseURL: "https://apptalents-bfdcf.firebaseio.com",
    projectId: "apptalents-bfdcf",
    storageBucket: "apptalents-bfdcf.appspot.com",
    messagingSenderId: "818121816686",
    appId: "1:818121816686:web:89f70e3cc8dfa63903447f",
    measurementId: "G-DGNF871WJ4"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
